export type EQUIPMENT = "Dishwasher" | "Hob" | "Oven" | "Refrigerator" | "Washer" | "Other";

export interface IReview {
    addresser: string;
    city: string;
    date: string;
    text: string;
}

// for price list in main page
export interface IPriceList {
    title: string;
    eq_type: string;
    cells: IService[];
}

export interface IMalfunction {
    pk: string;
    title: string;
    icon_name: string;
    problems: IService[];
}

export interface IMalfunctionType {
    title: string;
    pk: string;
}

export interface IService {
    title: string;
    price: string;
}

export interface MainState {
    fetching: boolean;
    error: Object | undefined;
    reviews: IReview[];
    main_price_list: IPriceList[];
    malfunctions: IMalfunction[];
    malfunction_types: IMalfunctionType[];
    success_ld: boolean;
}


export const START_FETCH = "START_FETCH";
export const FETCH_ERROR = "FETCH_ERROR";

export const GET_REVIEWS = "GET_REVIEWS";
export const LEAVE_DETAILS = "LEAVE_DETAILS";
export const SWITCH_FLAG = "SWITCH_FLAG";
export const GET_MALFUNCTIONS = "GET_MALFUNCTIONS";
export const GET_MAIN_PRICELIST = "GET_MAIN_PRICELIST";


export interface StartFetchAction {
    type: typeof START_FETCH;
}

interface FetchActionError {
    type: typeof FETCH_ERROR;
    payload?: { error: Object | undefined }
}

interface GetReviews {
    type: typeof GET_REVIEWS;
    payload: IReview[];
}

interface GetMalfunctions {
    type: typeof GET_MALFUNCTIONS;
    payload: IMalfunction[];
}

interface GetMainPricelist {
    type: typeof GET_MAIN_PRICELIST;
    payload: IPriceList[];
}

interface SwitchFlag {
    type: typeof SWITCH_FLAG;
    payload: { field: "success_ld" | "", val: boolean }
}


export type MainActionTypes = StartFetchAction 
                              | FetchActionError
                              | GetMalfunctions
                              | GetMainPricelist
                              | SwitchFlag
                              | GetReviews;
