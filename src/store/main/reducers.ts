import { 
    START_FETCH, FETCH_ERROR, MainState, 
    MainActionTypes, GET_MAIN_PRICELIST, 
    GET_MALFUNCTIONS, SWITCH_FLAG, GET_REVIEWS, IMalfunction
} from './types';


const initialState: MainState = {
    fetching: false,
    error: undefined,
    reviews: [],
    malfunctions: [],
    malfunction_types: [],
    main_price_list: [],
    success_ld: false
};

export function mainReducer(
    state = initialState,
    action: MainActionTypes
): MainState {
    switch (action.type) {
        case START_FETCH:
            return {
                ...state,
                fetching: true
            };

        case GET_REVIEWS:
            return {
                ...state,
                reviews: action.payload,
                fetching: false
            }

        case GET_MAIN_PRICELIST:
            return {
                ...state,
                fetching: false,
                main_price_list: action.payload
            }

        case GET_MALFUNCTIONS:
            return {
                ...state,
                fetching: false,
                malfunctions: action.payload,
                malfunction_types: action.payload.map((malf: IMalfunction) => {
                    return {title: malf.title, pk: malf.pk}
                })
            }

        case SWITCH_FLAG:
            return {
                ...state,
                fetching: false,
                [action.payload.field]: action.payload.val
            }

        case FETCH_ERROR:
            return {
                ...state,
                fetching: false,
                error: action.payload
            };


        default:
            return state;

    }
}