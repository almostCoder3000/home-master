import config from "./configure";
import axios from 'axios';

async function postRequest(url:string, data:any) {    
    return axios({
        method: 'post',
        url: config.urlServer + url,
        data: data,
    }).then((res) => {
        return res.data;
    }).catch(err => {
        console.log('Error', err.response.status)
    });
}

async function getRequest(url:string, params:any) {
    let url_fetch = config.urlServer + url + "?";
    Object.keys(params).forEach((key:string) => url_fetch += `${key}=${params[key]}&`);
    url_fetch = url_fetch.slice(0, -1);

    return axios.get(url_fetch).then(resp => {
        return resp.data
    }).catch(err => {
        console.log('Error', err.response.status)
    });
}


export {postRequest, getRequest};
