import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Router } from 'react-router-dom';
import Switcher from './Switcher';
import './assets/scss/index.scss';
import ScrollToTop from './components/ScrollToTop';
import {createBrowserHistory} from 'history';
import ReactGA from 'react-ga';
import TagManager from 'react-gtm-module';
import { Provider } from 'react-redux';
import configureStore from './store';
import { YMInitializer } from 'react-yandex-metrika';

const tagManagerArgs = {
    gtmId: 'GTM-TQZPXRX'
};
 
TagManager.initialize(tagManagerArgs);

ReactGA.initialize('UA-164039795-1');

const history = createBrowserHistory();

ReactGA.set({ page: window.location.pathname });
ReactGA.pageview(window.location.pathname);

history.listen((location, action) => {
    ReactGA.set({ page: location.pathname });
    ReactGA.pageview(location.pathname);
});

const store = configureStore();

ReactDOM.render(
    <React.StrictMode>
        <YMInitializer accounts={[69106792]} options={{
            webvisor: true, clickmap: true, 
            trackLinks: true, accurateTrackBounce:true
        }}/>
        <Provider store={store}>
            <Router history={history} >
                <ScrollToTop />
                <Switcher />
            </Router>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
