import React, { Component } from 'react';
import CTAModal from './modals/CTAModal';
import { leaveDetails } from '../store/main/actions';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../store';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface props extends RouteComponentProps {
    other_problems: IOtherProblem[];
    leaveDetails: (
        cb:(res:any) => void, name:string, phone_number:string
    ) => Promise<void>;
    isFetching: boolean;
}

interface state {
    isShowingAll: boolean;
    showMoreHeight: number | undefined;
}

class OtherProblems extends Component<props, state> {
    state = {
        isShowingAll: false,
        showMoreHeight: undefined
    }
    cta_modal: CTAModal | null;

    componentDidMount() {
        this.setState({
            showMoreHeight: document.getElementsByClassName("other-problems__list_item")[0].scrollHeight*2 + 20
        })
    }

    _renderProblems() {
        return this.props.other_problems.map((other_problem:IOtherProblem, ind:number) => {
            return (
                <div key={ind} className="other-problems__list_item">
                    <div 
                        className={`other-problems__list_item__header medium ${other_problem.icon_name}`}
                    >
                        {other_problem.title}
                    </div>
                    <div className="other-problems__list_item__body">
                        <p className="medium">{other_problem.problem_name}<span>от {other_problem.price}Р</span></p>
                        <p className="description">{other_problem.description}</p>
                        <a onClick={this.cta_modal?.modal?.showModal} className="btn">Вызвать мастера</a>
                    </div>
                </div>
            )
        })
    }

    _showMore(e:React.MouseEvent<HTMLParagraphElement>) {
        let allProblemList = document.getElementsByClassName("other-problems__list")[0];
        let firstProblemHeight:number = document.getElementsByClassName("other-problems__list_item")[0].scrollHeight;
        this.setState({ 
            isShowingAll: !this.state.isShowingAll,
            showMoreHeight: !this.state.isShowingAll ? allProblemList.scrollHeight : firstProblemHeight + 10
        });
    }

    render() {
        return (
            <div className="other-problems" id="our_services">
                <h1 className="light"><span className="bold">С каким устройством у вас</span> проблема?</h1>
                <div 
                    className="other-problems__list"
                    style={{
                        maxHeight: window.innerWidth > 767 ? this.state.showMoreHeight : undefined
                    }}
                >
                    {this._renderProblems.bind(this)()}
                </div>
                <p 
                    className={`show-more medium ${this.state.isShowingAll ? "active" : ""}`}
                    onClick={this._showMore.bind(this)}
                >{this.state.isShowingAll ? "Скрыть" : "Показать больше"}</p>
                <CTAModal 
                    isFetching={this.props.isFetching}
                    leaveDetails={this.props.leaveDetails}
                    ref={(node) => {this.cta_modal = node}} 
                    goToThankyou={() => {this.props.history.replace("/thankyou");}}
                />
            </div>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    isFetching: state.main.fetching
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	leaveDetails: async (
        cb:(res:any) => void, name:string, phone_number:string
    ) => {
		dispatch(leaveDetails(
            cb, name, phone_number
        ));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(OtherProblems));

export interface IOtherProblem {
    title: string;
    problem_name: string;
    price: string;
    description: string;
    icon_name: string;
}