import React, { Component, ComponentState } from 'react';
import ModalContainer from './ModalContainer';
import ReactGA from 'react-ga';
import InputMask from 'react-input-mask';
import axios from 'axios';
import ym from 'react-yandex-metrika';

ReactGA.initialize('UA-164039795-1');

interface props {
    leaveDetails: (
        cb:(res:any) => void, name:string, phone_number:string
    ) => Promise<void>;
    goToThankyou: () => void;
    isFetching: boolean;
}

interface state {
    name:string;
    phone_number:string;
    errors: {
        name: string;
        phone_number: string;
    };
    isFormValid: boolean;
    isInfoContent: boolean;
    info: {
        title: string;
        description: string;
    }
}

export default class CTAModal extends Component<props, state> {
    state = {
        name: "",
        phone_number: "",
        errors: {
            name: "",
            phone_number: ""
        },
        isFormValid: false,
        isInfoContent: false,
        info: {
            title: "",
            description: ""
        }
    }
    modal:ModalContainer | null;

    onHandleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        let {name, value} = e.target;
        if (name === 'phone_number') {
            value = `${value.substring(0, 3)} ${value.substring(3, 6)} ${value.substring(6, value.length)}`;
        }
        this.setState(
            {[name] : value} as ComponentState, 
            () => this.validatingForm(name)
        );
    }

    setError = (field: "name" | "phone_number", mess: string) => {
        this.setState({
            errors: {
                ...this.state.errors,
                [field]: mess
            }
        }, () => {
            let lenErrors:number = 0;
            for (let field in this.state.errors) {
                lenErrors += Reflect.get(this.state.errors, field).length;            
            }
            this.setState({isFormValid: lenErrors === 0})
        })
    }

    validatingForm = async (field: string) => {
        switch(field) {
            case "name":
                await this.setError("name", 
                    this.state.name.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;
            
            case "phone_number":
                await this.setError("phone_number", 
                    this.state.phone_number.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;

            default:
                break;
        }
    }

    onAfterSubmit = (res:string) => {
        if (res === "ok") {
            /*this.setState({
                isInfoContent: true,
                info: {
                    title: "Заявка успешно отправлена!",
                    description: "Мы перезвоним Вам в течение 15 минут. Наш специалист ответит на все интересующие вопросы."
                }
            });*/
            ReactGA.event({
                category: "Отправка контактов",
                action: "От правка без почты и неисправности"
            });
            this.modal?.hideModal();
            this.props.goToThankyou();
        } else {
            this.setState({
                isInfoContent: true,
                info: {
                    title: "Произошла ошибка...",
                    description: "Попробуйте заполнить форму ещё раз."
                }
            });
        }
    }

    onHide = () => {
        this.setState({
            name: "",
            phone_number: "",
            errors: {
                name: "",
                phone_number: ""
            },
            isFormValid: false,
            isInfoContent: false
        })
    }

    onSubmit = async () => {
        for (let field in this.state.errors) {
            await this.validatingForm(field);          
        }
        if (this.state.isFormValid) {
            this.props.leaveDetails(
                this.onAfterSubmit,
                this.state.name,
                this.state.phone_number
            );
            ym('reachGoal', 'all_form');
        }
    }

    _renderFormContent() {
        return (
            <>
            <h3>Оставьте заявку сейчас и получите скидку 20% на ремонт</h3>
                <div className="form">
                    <div className="fields">
                        <div className={`wrapper ${this.state.errors.name.length > 0 ? "error" : ""}`}>
                            <input 
                                type="text" 
                                placeholder="Ваше имя" 
                                name="name"
                                value={this.state.name}
                                onChange={this.onHandleChange}
                            />
                            <span>{this.state.errors.name}</span>
                        </div>
                        <div className={`wrapper ${this.state.errors.phone_number.length > 0 ? "error" : ""}`}>
                            <InputMask
                                name="phone_number"
                                type="tel"
                                placeholder="+7 (___)__-__-___" 
                                mask="+7 (999) 999-99-99"
                                value={this.state.phone_number}
                                onChange={this.onHandleChange}/>
                            <span>{this.state.errors.phone_number}</span>
                        </div>
                        
                    </div>
                    <div className="actions">
                        <p className="light">Нажимая на кнопку, вы 
                            соглашаетесь с <br/>
                            <a href="/privacy-policy">политикой обработки персональных данных</a>
                        </p>
                        <button 
                            className="btn" 
                            disabled={this.props.isFetching} 
                            onClick={this.onSubmit}
                        >Отправить</button>
                    </div>
                </div>
            </>
        )
    }

    _renderInfoContent() {
        return (
            <>
            <h3>{this.state.info.title}</h3>
            <p>{this.state.info.description}</p>
            <button className="btn" onClick={this.modal?.hideModal}>ОК</button>
            </>
        )
    }

    render() {
        
        return (
            <ModalContainer 
                ref={(node) => this.modal = node} 
                className="cta-modal"
                onHide={this.onHide}
            >
                <div 
                    className="close-btn"
                    onClick={this.modal?.hideModal}
                >&times;</div>
                {this.state.isInfoContent ? this._renderInfoContent() : this._renderFormContent()}
            </ModalContainer>
        )
    }
}
