import React, { Component, ComponentState } from 'react';
import ModalContainer from './ModalContainer';

interface props {
    isFetching: boolean;
    sendReview: (
        addresser:string, city:string, 
        text:string, cb:(res:any) => void
    ) => Promise<void>;
}

interface state {
    addresser: string;
    city: string;
    text: string;
    errors: {
        addresser: string;
        city: string;
        text: string;
    },
    isFormValid: boolean;
    isInfoContent: boolean;
    info: {
        title: string;
        description: string;
    }
}

const emptyState = {
    addresser: "",
    city: "",
    text: "",
    errors: {
        addresser: "",
        city: "",
        text: ""
    },
    isFormValid: false,
    isInfoContent: false,
    info: {
        title: "",
        description: ""
    }
}

export default class ReviewModal extends Component<props, state> {
    state = emptyState;
    modal:ModalContainer | null;

    onHandleChange = (e:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        let {name, value} = e.target;
        
        this.setState(
            {[name] : value} as ComponentState, 
            () => this.validatingForm(name)
        );
    }

    setError = (field: "addresser" | "city" | "text", mess: string) => {
        this.setState({
            errors: {
                ...this.state.errors,
                [field]: mess
            }
        }, () => {
            let lenErrors:number = 0;
            for (let field in this.state.errors) {
                lenErrors += Reflect.get(this.state.errors, field).length;            
            }
            this.setState({isFormValid: lenErrors === 0})
        })
    }

    validatingForm = async (field: string) => {
        switch(field) {
            case "addresser":
                await this.setError("addresser", 
                    this.state.addresser.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;
            
            case "city":
                await this.setError("city", 
                    this.state.city.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;
            
            case "text":
                await this.setError("text", 
                    this.state.text.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;

            default:
                break;
        }
    }

    onAfterSubmit = (res:string) => {
        if (res === "ok") {
            this.setState({
                isInfoContent: true,
                info: {
                    title: "Отзыв успешно отправлен",
                    description: "Спасибо, что делаете нас лучше!"
                }
            });
        } else {
            this.setState({
                isInfoContent: true,
                info: {
                    title: "Произошла ошибка...",
                    description: "Попробуйте заполнить форму ещё раз."
                }
            });
        }
    }

    onSubmit = async () => {
        for (let field in this.state.errors) {
            await this.validatingForm(field);          
        }
        if (this.state.isFormValid) {
            this.props.sendReview(
                this.state.addresser,
                this.state.city,
                this.state.text,
                this.onAfterSubmit
            );
        }
    }

    onHide = () => {
        this.setState(emptyState)
    }

    _renderFormContent = () => {
        return (
            <>
            <h3>Здесь вы можете оставить свой отзыв о нас</h3>
            <div className="form">
                <div className="fields">
                    <div className={`wrapper ${this.state.errors.addresser.length > 0 ? "error" : ""}`}>
                        <input 
                            type="text" 
                            placeholder="Как Вас зовут?" 
                            name="addresser"
                            value={this.state.addresser}
                            onChange={this.onHandleChange}
                        />
                        <span>{this.state.errors.addresser}</span>
                    </div>
                    <div className={`wrapper ${this.state.errors.city.length > 0 ? "error" : ""}`}>
                        <input 
                            type="text" 
                            placeholder="Откуда Вы?" 
                            name="city"
                            value={this.state.city}
                            onChange={this.onHandleChange}
                        />
                        <span>{this.state.errors.city}</span>
                    </div>
                    <div className={`wrapper ${this.state.errors.text.length > 0 ? "error" : ""}`}>
                        <textarea 
                            placeholder="Текст отзыва" 
                            name="text"
                            value={this.state.text}
                            onChange={this.onHandleChange}
                        />
                        <span>{this.state.errors.text}</span>
                    </div>
                </div>
                <div className="actions">
                    <button 
                        className="btn" 
                        disabled={this.props.isFetching} 
                        onClick={this.onSubmit}
                    >Отправить</button>
                </div>
            </div>
            </>
        )
    }

    _renderInfoContent() {
        return (
            <>
            <h3>{this.state.info.title}</h3>
            <p>{this.state.info.description}</p>
            <button className="btn" onClick={this.modal?.hideModal}>ОК</button>
            </>
        )
    }

    render() {
        return (
            <ModalContainer 
                ref={(node) => this.modal = node} 
                className="cta-modal"
            >
                <div 
                    className="close-btn"
                    onClick={this.modal?.hideModal}
                >&times;</div>
                {this.state.isInfoContent ? this._renderInfoContent() : this._renderFormContent()}
            </ModalContainer>
        )
    }
}
