import React, { Component } from 'react';
import ModalContainer from './ModalContainer';

interface state {
    title: string;
    description: string;
    closeAction?: () => void;
}

export default class InfoModal extends Component<{}, state> {
    modal:ModalContainer | null;

    state:state = {
        title: "",
        description: ""
    }

    showModal(title:string, description:string, closeAction?:() => void) {
        this.modal?.showModal();
        this.setState({ title, description, closeAction });
    }

    render() {
        return (
            <ModalContainer 
                ref={(node) => this.modal = node} 
                className="info-modal"
                onHide={this.state.closeAction}
            >
                <div 
                    className="close-btn"
                    onClick={this.modal?.hideModal}
                >&times;</div>
                <h3>{this.state.title}</h3>
                <p>{this.state.description}</p>
                <button 
                    className="btn" 
                    onClick={this.modal?.hideModal}
                >ОК</button>
            </ModalContainer>
        )
    }
}
