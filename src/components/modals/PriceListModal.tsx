import React, { Component } from 'react';
import ModalContainer from './ModalContainer';
import { IService } from '../../store/main/types';

interface state {
    header_title: string;
    header_icon: string;
    list: IService[];
}

export default class PriceListModal extends Component<{}, state> {
    modal:ModalContainer | null;

    state = {
        header_title: "",
        header_icon: "",
        list: []
    }

    showModal(header_title:string, header_icon:string, list:IService[]) {
        this.modal?.showModal();
        //list = list.concat(list).concat(list);
        this.setState({header_icon, header_title, list});
    }

    _renderList() {
        return this.state.list.map((item:IService, ind:number) => {
            return (
                <div className="item" key={ind}>
                    <p className="medium">{item.title}</p>
                    <span className="medium">от {item.price}Р</span>
                </div>
            )
        })
    }

    render() {
        return (
            <ModalContainer 
                ref={(node) => this.modal = node} 
                className="price-list-modal"
            >
                <div 
                    className="close-btn"
                    onClick={this.modal?.hideModal}
                >&times;</div>
                <div className="price-list-modal__header">
                    <p className={`${this.state.header_icon} bold`}>{this.state.header_title}</p>
                </div>
                <div className="price-list-modal__body">
                    {this._renderList.bind(this)()}
                </div>
                <div className="price-list-modal__action">
                    <a href="" className="btn" type-btn="ghost">Вызвать мастера</a>
                </div>
            </ModalContainer>
        )
    }
}
