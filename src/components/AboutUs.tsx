import React, { Component } from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import first from '../assets/imgs/desktop/about-company-1.jpg';
import second from '../assets/imgs/desktop/about-company-2.jpg';
import third from '../assets/imgs/desktop/about-company-3.jpg';

export default class AboutUs extends Component {
    render() {
        const handleOnDragStart = (e:any) => e.preventDefault();
        return (
            <div className="about-us" id="about_company">
                <h1 className="light"><span className="bold">О нашей</span> компании</h1>
                <div className="for-swap">
                    <div className="description">
                        <p className="light">
                            <span className="medium">Компания «Хоум-Мастер» </span> 
                            чинит бытовую технику в Екатеринбурге с 2016 года. За это 
                            время мы вернули в строй свыше 30 000 единиц бытовой техники, 
                            в том числе 10 000 стиральных машин автомат.
                        </p>
                        <p className="light">
                            <span className="medium">
                                В пределах 30 км от города наш мастер приедет и 
                                проведёт комплексную диагностику бесплатно в случае 
                                дальнейшей работы. 
                            </span> 
                            После телефонной консультации Мастер возьмёт с собой 
                            нужные запчасти, поэтому в 93% случаев сможет починить 
                            технику за один визит.
                        </p>
                        <p className="light">
                            <span className="medium">
                                3 года — официальная гарантия на все проведённые 
                                работы нашими специалистами. 
                            </span> 
                            В 100% случаев мы подтверждаем качество документально: 
                            после ремонта вы получаете гарантийный талон на детали и 
                            произведённые работы по форме БО-1.
                        </p>
                    </div>
                    <div className="gallery">
                        <div className="img first"></div>
                    </div>
                </div>
                <div className="virtues">
                    <div className="virtues__item qualified-specialists">
                        <h3>Квалифицированные специалисты</h3>
                        <p className="light">
                            Все мастера нашей компании имеют сертификаты, 
                            подтверждающие их высокую квалификацию.
                        </p>
                    </div>
                    <div className="virtues__item repair-models">
                        <h3>Ремонт любых моделей</h3>
                        <p className="light">
                            В распоряжении мастеров всегда высокоточное 
                            диагностическое оборудование и профессиональный 
                            инструмент, поэтому мы можем гарантировать качественный 
                            ремонт Вашей бытовой техники.
                        </p>
                    </div>
                    <div className="virtues__item warehouse">
                        <h3>Собственный склад запчастей</h3>
                        <p className="light">
                            Наши специалисты работают исключительно с 
                            оригинальными деталями от производителя, которые 
                            всегда в наличии на собственном складе в Екатеринбурге.
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}
