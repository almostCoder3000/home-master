import React, { Component } from 'react';
import PriceListModal from './modals/PriceListModal';
import { IMalfunction, IService } from '../store/main/types';
import CTAModal from './modals/CTAModal';
import { connect } from 'react-redux';
import { leaveDetails } from '../store/main/actions';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../store';
import ym from 'react-yandex-metrika';
import { RouteComponentProps, withRouter } from 'react-router-dom';


interface props extends RouteComponentProps {
    problem_types: IMalfunction[];
    leaveDetails: (
        cb:(res:any) => void, name:string, phone_number:string
    ) => Promise<void>;
    isFetching: boolean;
}

interface state {
    isShowingAll: boolean;
    showMoreHeight: number | undefined;
}

class WhatProblem extends Component<props, state> {
    state = {
        isShowingAll: false,
        showMoreHeight: undefined,
    }
    price_list_modal:PriceListModal | null;
    cta_modal:CTAModal | null;

    componentDidUpdate(prevProps:props) {
        if (this.props.problem_types !== prevProps.problem_types){
            let items = document.getElementsByClassName("problems__header");
            for (let i = 0; i < items.length; i++) {            
                items[i].addEventListener('click', function() {                
                    items[i].classList.toggle('active');
                    let content:any = items[i].nextElementSibling;
                    
                    content.style.maxHeight = content.style.maxHeight ? null : content.scrollHeight + 100 + 'px';
                })
            }
            this.setState({
                showMoreHeight: document.getElementsByClassName("problems")[0].children[0].children[0].scrollHeight + 26
            })
        }
    }

    componentWillUnmount() {
        this.setState({
            isShowingAll: false,
            showMoreHeight: undefined
        })
    }

    openPriceListModal(title:string, icon:string, problems:IService[]) {
        this.price_list_modal?.showModal(title, icon, problems);
    }

    _renderProblems() {
        return this.props.problem_types.map((problem_type:IMalfunction, ind:number) => {
            return (
                <li key={ind}>
                    <div 
                        className={`problems__header medium ${problem_type.icon_name}`}
                    >
                        {problem_type.title}
                    </div>
                    <div className="problems__body">
                        {problem_type.problems.map((problem:IService, ind_p:number) => {
                            return (
                                <div className="problems__body_item" key={ind_p}>
                                    <p className="medium">{problem.title}</p>
                                    <span className="medium">от {problem.price}Р</span>
                                </div>
                            )
                        })}
                        <a  onClick={this.cta_modal?.modal?.showModal} className="btn">Вызвать мастера</a>
                        <a onClick={
                                this.openPriceListModal.bind(
                                    this, problem_type.title, 
                                    problem_type.icon_name,
                                    problem_type.problems
                                )
                            } 
                            className="link medium"
                        >Весь прайс-лист</a>
                    </div>
                </li>
            )
        })
    }

    _showMore(e:React.MouseEvent<HTMLParagraphElement>) {
        let allProblemList = document.getElementsByClassName("problems")[0].children[0];
        let firstProblemHeight:number = allProblemList.children[0].scrollHeight;
        this.setState({ 
            isShowingAll: !this.state.isShowingAll,
            showMoreHeight: !this.state.isShowingAll ? allProblemList.scrollHeight : firstProblemHeight + 10
        });
    }

    render() {
        return (
            <div className="what-problem" id="our_services">
                <h1 className="light"><span className="bold">Какая у вас</span> проблема?</h1>
                <div 
                    className="problems"
                    style={{
                        maxHeight: window.innerWidth > 767 ? this.state.showMoreHeight : undefined
                    }}
                >
                    <ul>
                        {this._renderProblems.bind(this)()}
                    </ul>
                </div>
                <p 
                    className={`show-more medium ${this.state.isShowingAll ? "active" : ""}`}
                    onClick={this._showMore.bind(this)}
                >{this.state.isShowingAll ? "Скрыть" : "Показать больше"}</p>
                <PriceListModal 
                    ref={(node) => this.price_list_modal = node} 
                />
                <CTAModal 
                    isFetching={this.props.isFetching}
                    leaveDetails={this.props.leaveDetails}
                    ref={(node) => {this.cta_modal = node}} 
                    goToThankyou={() => {this.props.history.replace("/thankyou");}}
                />
            </div>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    isFetching: state.main.fetching
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	leaveDetails: async (
        cb:(res:any) => void, name:string, phone_number:string
    ) => {
		dispatch(leaveDetails(
            cb, name, phone_number
        ));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(WhatProblem))
