import React, { Component } from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import { IReview } from '../store/main/types';
import { ThunkDispatch } from 'redux-thunk';
import { connect } from 'react-redux';
import { getReviews, sendReview } from '../store/main/actions';
import { AppState } from '../store';
import ReviewModal from './modals/ReviewModal';

interface props {
    reviews: IReview[];
    getReviews: () => Promise<void>;
    sendReview: (
        addresser:string, city:string, 
        text:string, cb:(res:any) => void
    ) => Promise<void>;
    isFetching: boolean;
}

class Reviews extends Component<props, {}> {
    modal:ReviewModal | null;

    componentDidMount() {
        this.props.getReviews();
    }

    _renderReviews() {        
        const handleOnDragStart = (e:any) => e.preventDefault();
        return this.props.reviews.map((review:IReview, key:number) => {
            return (
                <div className="reviews__list__item" key={key} onDragStart={handleOnDragStart}>
                    <div className="header">
                        <h3>{review.addresser}, <span>{review.city}</span></h3>
                        <span className="date">{review.date}</span>
                    </div>
                    <p className="light">{review.text}</p>
                </div>
            )
        })
    }

    openModal = () => {
        this.modal?.modal?.showModal();
    }

    render() {
        return (
            <>
            <div className="reviews" id="reviews">
                <h1 className="light">
                    <span><span className="bold">Что говорят</span> о нас</span>
                    <button 
                        className="btn" 
                        type-btn="ghost"
                        onClick={this.openModal}
                    ><span>Оставить отзыв</span></button>
                </h1>
                <div className="reviews__list">
                    <AliceCarousel 
                        mouseTrackingEnabled
                        responsive={{767: {items: 2}, 1199: {items: 3}}}
                    >
                        {this._renderReviews.bind(this)()}
                    </AliceCarousel>
                    
                </div>
            </div>
            <ReviewModal 
                isFetching={this.props.isFetching}
                sendReview={this.props.sendReview}
                ref={node => this.modal = node}
            />
            </>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    reviews: state.main.reviews,
    isFetching: state.main.fetching
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	getReviews: async () => {
        dispatch(getReviews());
    },
    sendReview: async (
        addresser:string, city:string, text:string, cb:(res:any) => void
    ) => {
        dispatch(sendReview(addresser, city, text, cb));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Reviews);
