import React, { Component } from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';

export default class Brands extends Component {
    render() {
        const handleOnDragStart = (e:any) => e.preventDefault();
        const brands = [(
            <div className="row" onDragStart={handleOnDragStart} key="0">
                <div className="brands__list__item aeg"></div>
                <div className="brands__list__item asko"></div>
                <div className="brands__list__item beko"></div>
                <div className="brands__list__item blomberg"></div>
                <div className="brands__list__item bosch"></div>
                <div className="brands__list__item brandt"></div>
                <div className="brands__list__item candy"></div>
                <div className="brands__list__item daewoo"></div>
                <div className="brands__list__item delonghi"></div>
                <div className="brands__list__item electrolux"></div>
                <div className="brands__list__item elenberg-1"></div>
                <div className="brands__list__item eurolux"></div>
            </div>),
            (<div className="row" onDragStart={handleOnDragStart} key="1">
                <div className="brands__list__item fagor"></div>
                <div className="brands__list__item flavia"></div>
                <div className="brands__list__item gaggenau"></div>
                <div className="brands__list__item general-electric"></div>
                <div className="brands__list__item gorenje"></div>
                <div className="brands__list__item haier"></div>
                <div className="brands__list__item hansa"></div>
                <div className="brands__list__item hoover"></div>
                <div className="brands__list__item hotpoint-ariston"></div>
                <div className="brands__list__item ikea"></div>
                <div className="brands__list__item indesit"></div>
                <div className="brands__list__item kaiser"></div>
            </div>),
            (<div className="row" onDragStart={handleOnDragStart} key="2">
                <div className="brands__list__item korting"></div>
                <div className="brands__list__item krona"></div>
                <div className="brands__list__item kuppersberg"></div>
                <div className="brands__list__item kuppersbusch"></div>
                <div className="brands__list__item lg"></div>
                <div className="brands__list__item lex"></div>
                <div className="brands__list__item mach"></div>
                <div className="brands__list__item miele"></div>
                <div className="brands__list__item neff"></div>
                <div className="brands__list__item samsung"></div>
                <div className="brands__list__item sharp"></div>
                <div className="brands__list__item siemens-1"></div>
            </div>),
            (<div className="row" onDragStart={handleOnDragStart} key="3">
                <div className="brands__list__item smeg"></div>
                <div className="brands__list__item vestel"></div>
                <div className="brands__list__item vestfrost"></div>
                <div className="brands__list__item whirlpool"></div>
                <div className="brands__list__item zanussi"></div>
                <div className="brands__list__item zerowatt"></div>
                <div className="brands__list__item zigmund"></div>
            </div>)
        ];
        return (
            <div className="brands">
                    <h1 className="light"><span className="bold">Мы работаем</span> с брендами</h1>
                    <div className="brands__list">
                        <div className="carousel">
                            <AliceCarousel 
                                mouseTrackingEnabled
                                items={brands}
                            />
                        </div>
                        <div className="simple-list">
                            {brands}
                        </div>
                    </div>
                </div>
        )
    }
}
