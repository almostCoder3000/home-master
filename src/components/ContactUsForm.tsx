import React, { Component, ComponentState } from 'react';
import { connect } from 'react-redux';
import { leaveDetails } from '../store/main/actions';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../store';
import InfoModal from './modals/InfoModal';
import ReactGA from 'react-ga';
import InputMask from 'react-input-mask';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import axios from 'axios';
import ym from 'react-yandex-metrika';

ReactGA.initialize('UA-164039795-1');

interface props extends RouteComponentProps {
    leaveDetails: (
        cb:(res:any) => void, name:string, phone_number:string
    ) => Promise<void>;
    isFetching: boolean;
}

interface state {
    name:string;
    phone_number:string;
    errors: {
        name: string;
        phone_number: string;
    };
    isFormValid: boolean;
}

class ContactUsForm extends Component<props, state> {
    state = {
        name: "",
        phone_number: "",
        errors: {
            name: "",
            phone_number: ""
        },
        isFormValid: false
    }
    modal:InfoModal | null;

    onHandleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        let {name, value} = e.target;
        this.setState(
            {[name] : value} as ComponentState, 
            () => this.validatingForm(name)
        );
    }

    setError = (field: "name" | "phone_number", mess: string) => {
        this.setState({
            errors: {
                ...this.state.errors,
                [field]: mess
            }
        }, () => {
            let lenErrors:number = 0;
            for (let field in this.state.errors) {
                lenErrors += Reflect.get(this.state.errors, field).length;            
            }
            this.setState({isFormValid: lenErrors === 0})
        })
    }

    validatingForm = async (field: string) => {
        switch(field) {
            case "name":
                await this.setError("name", 
                    this.state.name.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;
            
            case "phone_number":
                await this.setError("phone_number", 
                    this.state.phone_number.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;

            default:
                break;
        }
    }

    onAfterSubmit = (res:string) => {
        if (res === "ok") {
            /*this.modal?.showModal(
                "Заявка успешно отправлена!",
                "Мы перезвоним Вам в течение 15 минут. Наш специалист ответит на все интересующие вопросы."
            )*/
            ReactGA.event({
                category: "Отправка контактов",
                action: "От правка без почты и неисправности"
            });
            this.props.history.replace("/thankyou");
        } else {
            this.modal?.showModal(
                "Произошла ошибка...",
                "Попробуйте заполнить форму ещё раз."
            )
        }
        this.setState({
            name: "",
            phone_number: "",
            errors: {
                name: "",
                phone_number: ""
            },
            isFormValid: false
        })
    }

    onSubmit = async () => {
        for (let field in this.state.errors) {
            await this.validatingForm(field);          
        }
        if (this.state.isFormValid) {
            this.props.leaveDetails(
                this.onAfterSubmit,
                this.state.name,
                this.state.phone_number
            );
            ym('reachGoal', 'all_form');
        }
    }

    render() {
        return (
            <div className="contact-us-form">
                <div className="card-container">
                    <div className="form">
                        <h1 className="light"><span className="bold">Остались </span>вопросы?</h1>
                        <p className="light">Заполните форму ниже, мы перезвоним Вам в течение 15 минут. 
                        Наш специалист ответит на все интересующие вопросы.</p>
                        <div className="fields">
                            <div className={`wrapper ${this.state.errors.name.length > 0 ? "error" : ""}`}>
                                <input 
                                    type="text" 
                                    placeholder="Ваше имя" 
                                    name="name"
                                    value={this.state.name}
                                    onChange={this.onHandleChange}
                                />
                                <span>{this.state.errors.name}</span>
                            </div>
                            <div className={`wrapper ${this.state.errors.phone_number.length > 0 ? "error" : ""}`}>
                                <InputMask
                                    name="phone_number"
                                    type="tel"
                                    placeholder="+7 (___)__-__-___" 
                                    mask="+7 (999) 999-99-99"
                                    value={this.state.phone_number}
                                    onChange={this.onHandleChange}/>
                                <span>{this.state.errors.phone_number}</span>
                            </div>
                        </div>
                        <div className="actions">
                            <button 
                                className="btn" 
                                onClick={this.onSubmit}
                                disabled={this.props.isFetching}
                            >Отправить</button>
                            <p className="light">Нажимая на кнопку, вы 
                                соглашаетесь с <br/>
                                <a href="/privacy-policy">политикой обработки персональных данных</a>
                            </p>
                        </div>
                    </div>
                    <div className="img"></div>
                </div>
                <InfoModal 
                    ref={(node) => this.modal = node} 
                />
            </div>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    isFetching: state.main.fetching
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	leaveDetails: async (
        cb:(res:any) => void, name:string, phone_number:string
    ) => {
		dispatch(leaveDetails(
            cb, name, phone_number
        ));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ContactUsForm));
