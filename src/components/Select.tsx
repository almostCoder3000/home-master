import React, { Component } from 'react';

interface props<T> {
    onChange: (selected: T) => void;
    options: T[];
    placeholder: string;
    value: T | null;
}

interface state {
    isOpen: boolean;
}

export default class Select<T extends ISelectItem> extends Component<props<T>, state> {
    state = {
        isOpen: false
    }
    
    _showItems() {
        this.setState({isOpen: !this.state.isOpen});
    }

    _selectItem(option:T) {
        this.props.onChange(option);
        this.setState({isOpen: false});
    }
    
    _renderSelectItems() {
        return this.props.options.map((option: T, ind:number) => {
            return (
                <li 
                    className="select-field__body_item" 
                    key={ind}
                    onClick={this._selectItem.bind(this, option)}
                >
                    {option.title}
                </li>
            )
        })
    }
    
    render() {
        return (
            <div className={`select-field ${this.state.isOpen ? "active": ""}`}>
                <div 
                    className="select-field__header"
                    onClick={this._showItems.bind(this)}
                >
                    {this.props.value ? this.props.value.title : "Выберите неисправность"}
                </div>
                <div className="select-field__body">
                    <ul>
                        {this._renderSelectItems.bind(this)()}
                    </ul>
                </div>
            </div>
        )
    }
}

interface ISelectItem {
    title: string;
}