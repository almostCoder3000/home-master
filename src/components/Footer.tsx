import React, {Component} from 'react';

export default class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="logo">©2020</div>
                <div className="information">
                    <p className="light">Екатеринбург, ул. Фурманова, 123 <br/>
                        Часы работы: c 7:30 - 22:00 <br/>
                        ООО “Джойсервис” ИНН 6679129485<br/>
                        <span>Сайт не является публичной офертой</span></p>
                    <p>
                        <span>+7 (343) 227-99-37</span><br/>
                        homemaster96@yandex.ru
                    </p>
                </div>
            </footer>
        )
    }
}
