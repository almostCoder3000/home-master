import React, { Component } from 'react';
import { HashLink as Link } from 'react-router-hash-link';
import ym from 'react-yandex-metrika';
import CTAModal from './modals/CTAModal';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { leaveDetails } from '../store/main/actions';
import { connect } from 'react-redux';
import ModalContainer from './modals/ModalContainer';

type props = {
    leaveDetails: (
        cb:(res:any) => void, name:string, phone_number:string
    ) => Promise<void>;
    isFetching: boolean;
} & RouteComponentProps;

type state = {
    isMenuOpen: boolean;
}

class Header extends Component<props, state> {
    cta_modal: CTAModal | null;
    shares_modal: ModalContainer | null;

    state = {
        isMenuOpen: false
    }

    closeMenu = () => {
        this.setState({isMenuOpen: false});
    }

    openMenu = () => {
        this.setState({isMenuOpen: true});
    }

    render() {
        return (
            <>
            <header>
                <Link to="/" className={`logo ${this.state.isMenuOpen ? "active" : ""}`}></Link>
                <ul className={`menu ${this.state.isMenuOpen ? "active" : ""}`}>
                    <li><Link to="#our_services" onClick={this.closeMenu}>Наши услуги</Link></li>
                    <li><Link to="#prices" onClick={this.closeMenu}>Цены</Link></li>
                    <li><Link to="#about_company" onClick={this.closeMenu}>О компании</Link></li>
                    <li><Link to="#reviews" onClick={this.closeMenu}>Отзывы</Link></li>
                    <li><Link to="" onClick={() => {
                        this.closeMenu();
                        this.shares_modal?.showModal();
                    }}>Акции</Link></li>
                    <div className="contacts">
                        <p>+7 (343) 364-15-68</p>
                        <span>homemaster96@yandex.ru</span>
                        <span className="light">Екатеринбург, ул. Фурманова, 123 <br/>
                    Часы работы: c 7:30 - 22:00</span>
                    </div>
                </ul>
                <div className="work-time">
                    <p>Работаем</p>
                    <span>24/7</span>
                </div>
                <a 
                    className="call-me mgo-number-21894" 
                    href="tel:+73433641568"
                    onClick={() => {
                        ym('reachGoal', 'Callme');
                        //this.cta_modal?.modal?.showModal();
                    }}
                >
                    <span>Позвонить</span>
                </a>
                <div 
                    className="burger" 
                    onClick={
                        this.state.isMenuOpen ? this.closeMenu : this.openMenu
                    }
                ></div>
                <span className="number">
                    <p><span className="bold">+7 (343) 364-15-68</span> - в дневное время <br/>
                    <span className="bold">+7 (343) 346-91-96</span> - в ночное время</p>
                </span>
            </header>
            <CTAModal 
                isFetching={this.props.isFetching}
                leaveDetails={this.props.leaveDetails}
                ref={(node) => {this.cta_modal = node}} 
                goToThankyou={() => {this.props.history.replace("/thankyou");}}
            />
            <ModalContainer 
                ref={(node) => this.shares_modal = node} 
                className="info-modal"
                onHide={() => {}}
            >
                <div 
                    className="close-btn"
                    onClick={this.shares_modal?.hideModal}
                >&times;</div>
                <h3>Актуальные акции</h3>
                <ul>
                    <li>Заказ на выезд мастера через сайт гарантирует скидку 10%</li>
                    <li>Счастливые часы с 10:00 до 12:00 утра дополнительная скидка 5%</li>
                    <li>Скидки пенсионерам при предоставлении пенсионного удостоверения 20%</li>
                    <li>Скидка при предоставлении флаера в размере 15%</li>
                </ul>
            </ModalContainer>
            </>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    isFetching: state.main.fetching
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	leaveDetails: async (
        cb:(res:any) => void, name:string, phone_number:string
    ) => {
		dispatch(leaveDetails(
            cb, name, phone_number
        ));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));