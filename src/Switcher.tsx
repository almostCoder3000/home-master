import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import MainPage from './pages/MainPage';
import WasherPage from './pages/WasherPage';
import DishwasherPage from './pages/Dishwasher';
import RefrigeratorPage from './pages/RefrigeratorPage';
import HobPage from './pages/HobPage';
import OtherPage from './pages/OtherPage';
import OvenPage from './pages/OvenPage';
import PrivacyPolicyPage from './pages/PrivacyPolicyPage';

export default class Switcher extends Component {
    render() {
        return (
            <Switch>
                <Route path="/washer">
                    <WasherPage />
                </Route>
                <Route path="/dishwasher">
                    <DishwasherPage />
                </Route>
                <Route path="/refrigerator">
                    <RefrigeratorPage />
                </Route>
                <Route path="/oven">
                    <OvenPage />
                </Route>
                <Route path="/hob">
                    <HobPage />
                </Route>
                <Route path="/other">
                    <OtherPage />
                </Route>
                <Route path="/thankyou">
                    <MainPage />
                </Route>
                <Route path="/privacy-policy">
                    <PrivacyPolicyPage />
                </Route>
                <Route path="/">
                    <MainPage />
                </Route>
            </Switch>
        )
    }
}
