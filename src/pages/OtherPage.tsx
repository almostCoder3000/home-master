import React, { Component } from 'react';
import Header from '../components/Header';
import MainBlock from '../components/MainBlock';
import AboutUs from '../components/AboutUs';
import Brands from '../components/Brands';
import Reviews from '../components/Reviews';
import Questionare from '../components/Questionare';
import ContactUsForm from '../components/ContactUsForm';
import Footer from '../components/Footer';
import CostCalculation from '../components/CostCalculation';
import OtherProblems, { IOtherProblem } from '../components/OtherProblems';
import { EQUIPMENT, IMalfunction } from '../store/main/types';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { getMalfunctions } from '../store/main/actions';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import PhoneFixedButton from '../components/PhoneFixedButton';

interface props extends RouteComponentProps {
    getMalfunctions: (equipment:EQUIPMENT) => Promise<void>;
    malfunctions: IMalfunction[];
}

class OtherPage extends Component<props, {}> {
    componentDidMount() {
        this.props.getMalfunctions("Other");
    }

    render() {
        return (
            <>
                <Header />
                <MainBlock
                    part_of_title={(<>телевизоров,<br/>ледогенераторов, кофемашин<br/></>)}
                    img_name="other"
                />
                <OtherProblems
                    other_problems={problems} 
                />
                <CostCalculation equipment_type="Other" eq_type_rus="Другая техника" />
                <AboutUs />
                <Brands />
                <Reviews />
                <Questionare />
                <ContactUsForm />
                <PhoneFixedButton />
                <Footer />
            </>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    malfunctions: state.main.malfunctions
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	getMalfunctions: async (equipment:EQUIPMENT) => {
		dispatch(getMalfunctions(equipment));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(OtherPage));

const problems:IOtherProblem[] = [
    {
        title: "Кофемашина",
        icon_name: "coffee-machine",
        problem_name: "Ремонт кофемашины",
        price: "270",
        description: `С более подробной информацией Вы можете ознакомиться по телефону`
    },
    {
        title: "Телевизор",
        icon_name: "tv",
        problem_name: "Ремонт телевизора",
        price: "650",
        description: `С более подробной информацией Вы можете ознакомиться по телефону`
    },
    {
        title: "Ледогенератор",
        icon_name: "other",
        problem_name: "Ремонт ледогенератора",
        price: "350",
        description: `С более подробной информацией Вы можете ознакомиться по телефону`
    }
];